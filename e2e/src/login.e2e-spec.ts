import { LoginPage } from './login.pageObject';
import { browser, logging, protractor } from 'protractor';

describe('Login', () => {
    let loginPage: LoginPage;

    beforeEach(() => {
        loginPage = new LoginPage();
    });

    it('should display app header', () => {
        loginPage.navigateTo();
        expect(loginPage.getLoginPageHeading()).toContain('Cisco App');
    });

    it('shuld login', async () => {
        browser.get('/login');
        await loginPage.fillUsername();
        await loginPage.fillPassword();
        await loginPage.clickLoginButton();
        browser.waitForAngular();
        expect(browser.driver.getCurrentUrl()).toMatch('http://localhost:4300/aws');
    });

    it('should login with enter', async () => {
        browser.get('/login');
        await loginPage.fillUsername();
        await loginPage.fillPassword();
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        browser.waitForAngular();
        expect(browser.driver.getCurrentUrl()).toMatch('http://localhost:4300/aws');
    });


    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
