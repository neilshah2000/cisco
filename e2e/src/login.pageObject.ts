import { browser, by, element } from 'protractor';

export class LoginPage {
    navigateTo() {
        // since auth gaurd is enabled, home page will actually be /login
        return browser.get(browser.baseUrl) as Promise<any>;
    }

    getTitleText() {
        return element(by.css('app-cisco')).getText() as Promise<string>;
    }

    getLoginPageHeading() {
        return element(by.css('app-cisco')).getText() as Promise<string>;
    }

    async fillUsername() {
        const usernameElement = element(by.name('username'));
        await usernameElement.clear();
        return usernameElement.sendKeys('test');
    }

    async fillPassword() {
        const passwordElement = element(by.name('password'));
        await passwordElement.clear();
        return passwordElement.sendKeys('test');
    }

    clickLoginButton() {
        return element(by.buttonText('Login')).click() as Promise<void>;
    }
}
