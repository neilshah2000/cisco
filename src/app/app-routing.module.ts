import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page/page.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './login/service/auth-guard.service';

export const routes: Routes = [
    {
        path: 'aws',
        component: PageComponent,
        canActivate: [AuthGuardService]
    }, {
        path: '',
        redirectTo: '/aws',
        pathMatch: 'full'
    }, {
        path: 'login',
        component: LoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
