import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { AwsService } from './services/aws.service';
import { HttpClientModule } from '@angular/common/http';
import { PageComponent } from './page/page.component';
import { LoginModule } from './login/login.module';
import { MaterialDesignModule } from './material-design.module';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        PageComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LoginModule,
        MaterialDesignModule
    ],
    providers: [AwsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
