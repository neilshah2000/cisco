import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialDesignModule } from './../material-design.module';
import { HeaderComponent } from './header.component';
import { LoginService } from './../login/service/login.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    const loginServiceStub = {
        logout: () => {
            return of([]);
        }
    };

    beforeEach(async(() => {
        spyOn(loginServiceStub, 'logout');
        TestBed.configureTestingModule({
            declarations: [ HeaderComponent ],
            imports: [MaterialDesignModule, RouterTestingModule],
            providers: [
                {
                    provide: Router,
                    useClass: class { navigate = jasmine.createSpy('navigate'); }
                },
                { provide: LoginService, useValue: loginServiceStub },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should logout user', async(() => {
        const logoutButton = fixture.debugElement.nativeElement.querySelector('button');
        logoutButton.click();
        fixture.whenStable().then(() => {
            expect(loginServiceStub.logout).toHaveBeenCalled();
        });
    }));
});
