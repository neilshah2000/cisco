import { Component, OnInit } from '@angular/core';
import { LoginService } from './service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username = '';
    password = '';

    constructor(
        private loginService: LoginService,
        private router: Router
    ) { }

    ngOnInit() {
    }

    login() {
        this.loginService.login(this.username, this.password).subscribe((response: any) => {
            if (response.loggedIn) {
                this.router.navigate(['/aws']);
            } else {
                // notification service to display notificatioon
                alert('incorrect username and password');
            }
        }, (err) => {
            // notification service to display notificatioon
            alert('incorrect username and password');
        });
    }
}
