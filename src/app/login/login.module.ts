import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { AuthGuardService } from './service/auth-guard.service';
import { SessionService } from './service/session.service';
import { LoginService } from './service/login.service';
import { MaterialDesignModule } from './../material-design.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule, BrowserModule, MaterialDesignModule, FormsModule
    ],
    providers: [AuthGuardService, SessionService, LoginService]
})
export class LoginModule { }
