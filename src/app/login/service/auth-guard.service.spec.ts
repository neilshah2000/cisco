import { TestBed } from '@angular/core/testing';
import { AuthGuardService } from './auth-guard.service';
import { Router } from '@angular/router';

describe('AuthGuardService', () => {
    const routerStub = {};

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            AuthGuardService,
            { provide: Router, useValue: routerStub },
        ]
    }));

    it('should be created', () => {
        const service: AuthGuardService = TestBed.get(AuthGuardService);
        expect(service).toBeTruthy();
    });
});
