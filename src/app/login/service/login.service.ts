import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { of, throwError } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
    fakeToken = 'userloggedinxxxxxxxx';
    actaulUsername = 'test';
    actualPassword = 'test';

    constructor(
        private sessionService: SessionService,
        private httpClient: HttpClient
    ) { }

    public login(username: string, password: string) {
        const url = '/api/login';
        const body = { username, password};
        return this.httpClient.post(url, body).pipe(
            tap((token: any) => {
                if (token.loggedIn) {
                    this.sessionService.setSession(token);
                }
            }), shareReplay());
    }

    public logout() {
        this.sessionService.invalidateSession();
    }
}
