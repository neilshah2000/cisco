import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

    private jwtTokenName = 'jwt_token';

    constructor() { }

    public setSession(token) {
        localStorage.setItem('token', token);
    }

    public getSessionToken() {
        return localStorage.getItem('token');
    }

    public invalidateSession() {
        localStorage.removeItem('token');
    }

    public hasSession() {
        // return true;
        return localStorage.getItem('token') !== null;
    }
}
