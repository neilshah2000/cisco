import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PageComponent } from './page.component';
import { Component } from '@angular/core';
import { MaterialDesignModule } from './../material-design.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AwsService } from './../services/aws.service';
import { of } from 'rxjs';

@Component({ selector: 'app-header', template: '' })
class HeaderStubComponent { }

describe('PageComponent', () => {
    let component: PageComponent;
    let fixture: ComponentFixture<PageComponent>;

    const awsServiceStub = {
        getEc2Instances: () => {
            return of([]);
        }
    };

    beforeEach(async(() => {
        spyOn(awsServiceStub, 'getEc2Instances').and.returnValues(of([]));
        TestBed.configureTestingModule({
            declarations: [ PageComponent, HeaderStubComponent ],
            imports: [MaterialDesignModule, HttpClientTestingModule],
            providers: [
                { provide: AwsService, useValue: awsServiceStub}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should get aws data', async(() => {
        fixture.whenStable().then(() => {
            expect(awsServiceStub.getEc2Instances).toHaveBeenCalled();
        });
    }));
});
