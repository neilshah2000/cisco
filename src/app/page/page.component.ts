import { Component, OnInit } from '@angular/core';
import { AwsService } from './../services/aws.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
    title = 'cisco';
    awsData;
    displayedColumns: string[] = ['name', 'id', 'type', 'state', 'availabilityZone', 'publicIp', 'privateIp'];

    constructor(private awsService: AwsService) {

    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.awsService.getEc2Instances().subscribe((data: any) => {
            this.awsData = data;
        }, () => {
            // error message
        });
    }
}
