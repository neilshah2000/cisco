import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AwsService {

    constructor(private http: HttpClient) { }

    public getEc2Instances() {
        const url = '/api/aws/ec2';
        return this.http.get(url);
    }
}
