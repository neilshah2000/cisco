const express = require('express')
const app = express()
var fs = require('fs');
var util = require('util');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.get('/api/aws/ec2', (req, res) => res.sendFile(__dirname + '/short.json'));

app.post('/api/login', (req, res) => {
    console.log(req.body);
    validUsername = 'test';
    validPassword = 'test';
    if(req.body.username === validUsername && req.body.password === validPassword) {
        res.send({
            loggedIn: true
        });
    } else {
        res.send({
            loggedIn: false
        });
    }
});

app.listen(3000, () => console.log('Cisco app listening on port 3000'));
